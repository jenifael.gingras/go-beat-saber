package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"github.com/codeclysm/extract/v3"
	"context"
	"flag"
)

const basePath = "/home/patate/.local/share/Steam/steamapps/common/Beat Saber/Beat Saber_Data/CustomLevels"

// downloadFile downloads a file from an url and return the local file path.
func downloadFile(fullURL string) (string, error) {
	fmt.Printf("Uploading: %v\n", fullURL)

	fileURL, err := url.Parse(fullURL)
	if err != nil {
		return "", err
	}

	path := fileURL.Path
	segments := strings.Split(path, "/")
	fileName := segments[len(segments)-1]

	// Create blank file
	file, err := os.Create(fileName)
	if err != nil {
		return "", err
	}

	client := http.Client{
		CheckRedirect: func(r *http.Request, via []*http.Request) error {
			r.URL.Opaque = r.URL.Path
			return nil
		},
	}

	resp, err := client.Get(fullURL)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	size, err := io.Copy(file, resp.Body)
	defer file.Close()
	log.Printf("Downloaded a file of size: %v Mo", size / (1024 * 1024))

	return file.Name(), err
}

// unzipMod extract the mod to the destination.
func unzipMod(src string, dest string) error {
	ctx := context.Background()
	file, err := os.Open(src)
	if err != nil {
		return err
	}
	defer file.Close()

	err = extract.Zip(ctx, file, dest, nil)
	return err
}

// upload manages a mod upload to the server.
func upload(w http.ResponseWriter, r *http.Request) {
	log.Println("Downloading mod.")
	fullURL := r.FormValue("url")
	filePath, err := downloadFile(fullURL)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	log.Println("Extracting mod.")
	modName := r.FormValue("name")
	modDir := basePath + "/" + modName
	err = unzipMod(filePath, modDir)
	if err != nil {
		fmt.Print("Error")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	log.Println("Mod extracted.")
}

func main() {
	var dirName string
	flag.StringVar(&dirName, "dir", ".", "The dir with the statics files.")

	flag.Parse()
	log.Printf("Serving: %v", dirName)
	http.Handle("/", http.FileServer(http.Dir(dirName)))

	http.HandleFunc("/upload", upload)

	log.Fatal(http.ListenAndServe("0.0.0.0:9090", nil))
}
